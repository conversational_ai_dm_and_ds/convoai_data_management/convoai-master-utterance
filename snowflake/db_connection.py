# Config to pull data from Nucleus 
import platform
from sqlite3 import ProgrammingError
import sys
import snowflake 
from snowflake.connector.pandas_tools import write_pandas
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.backends import default_backend
import os 
import pandas as pd 
# examine the platform 
env_platform = platform.system() 
# set up the environment variables
userid = os.environ.get('USERID', '')
idrsa = os.environ.get('IDRSA', '')
password = os.environ.get('PASSCODE', '')

class snowflake_connector: 
    def __init__(self, 
                    userid:str="PZLYH9", 
                    account: str='ally.us-east-1.privatelink',
                    warehouse:str='WH_TEAM_TECH_CONVOAI_ME', 
                    database:str='TEAM_TECH_CONVOAI_P', 
                    schema:str='CORE', 
                    role:str="", 
                    private_key:str="",
                    conn: str="",
                    error: str=''
                    ):
        '''
        feature_presentation_method: "binary", "counts", "TF-IDF"
        data: dataframe of training data 
        '''
        self.userid = userid
        self.account = account
        self.warehouse = warehouse
        self.database = database 
        self.schema = schema
        self.role = 'RL_'+ userid+'_P'
        self.private_key = private_key
        self.conn = conn 
        self.error = error
    def create_cursor(self, environment:str="", id_rsa_filename:str='id_rsa', passcode_filename:str='id_rsa_passcode', userid:str='' ): 
        userid = userid # for local environment, change to your user id
        if  (env_platform == 'Darwin'):   # local environment 
            userid = userid # for local environment, change to your user id 
            path = '/Users/'+userid+'/.ssh/' # double check your path where you save your id_rsa and id_rsa_passcode 
            pwd = open(path + passcode_filename).read().encode()
            pkey = open(path + id_rsa_filename).read().encode()
        elif (env_platform == 'Linux') & (environment == 'workbench'):   # Workbench
        # Note the path for workbench and deployment will be different 
        # userid and password are passed through setting up environment variables 
            path = '/mnt/efs/fs1/home/'+userid+'/.ssh/'
            pwd = open(path + passcode_filename).read().encode()
            pkey = open(path + id_rsa_filename).read().encode()
        elif (env_platform == 'Linux') & (environment == 'prod'):   # RStudio Connect
            path = ''
            pkey = open(path + id_rsa_filename).read().encode()
            pwd = password.encode()

        p_key = serialization.load_pem_private_key(
            pkey
            ,password = pwd
            ,backend=default_backend()
        )
        pkb = p_key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
        )
        
        self.private_key = pkb

        conn = snowflake.connector.connect(
            user = self.userid,
            account = self.account,
            warehouse = self.warehouse,
            database = self.database,
            schema = self.schema,
            role = self.role,
            private_key = self.private_key
        )
        #Snowflake data select and print
        self.conn = conn
    def execute_query(self, query): 
        cur = self.conn.cursor()
        try: 
            cur.execute(query)
            # print(f"INFO: Query Sccessfully!")
        except ProgrammingError as pe:
            print(f"Programming Error: {pe}")
            self.error = 'program_error'
    
    def query_to_dataframe(self, query): 
        cur = self.conn.cursor()
        try: 
            cur.execute(query)
            df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
            # print(f"INFO: Query Sccessfully - {len(df)} rows!")
            return df 
        except ProgrammingError as pe:
            print(f"Programming Error: {pe}")
            self.error = 'program_error'
        except: 
            return pd.DataFrame()
        return pd.DataFrame()
    
    def write_data(self, df, table_name): 
        success, nchunks, nrows, _ = write_pandas(self.conn, df, table_name)
        # print(success, nchunks)
    